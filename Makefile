EMCXX ?= em++

INCLUDES=$(glob inc/*)
BINS=spasm.js spasm.wasm
ACE_JS=vendor/ace-1.15.2-min-noconflict/ace.js

.PHONY: dist clean clean-dist

dist: $(BINS) spasm.html $(ACE_JS)
	make clean-dist
	mkdir -p dist
	cp $^ dist/

CXXFLAGS=-DNO_APPSIGN -DUSE_REUSABLES -DUNIXVER -DUSE_BUILTIN_FCREATE -O3 -flto

COM_SRC := Z80Assembler Z80Label CTextStream CZ80AssemblerTest
COM_SRC := $(addprefix src/,$(addsuffix .cpp,$(COM_SRC)))
SRC:=$(wildcard src/*.cpp)
# Exclude the COM interface from our build
SRC := $(filter-out $(COM_SRC),$(SRC))

OBJ = $(addsuffix .bc, $(basename $(SRC)))
OBJ_FILES = $(addsuffix .bc, $(basename $(notdir $(SRC))))

.SUFFIXES:
.SUFFIXES: .cpp .bc

.cpp.bc:
	$(EMCXX) $(CXXFLAGS) -o $@ -c $<

$(BINS) &: $(OBJ) Makefile spasm.pre.js spasm.post.js $(INCLUDES)
	emcc --pre-js spasm.pre.js --post-js spasm.post.js \
	     --embed-file inc/ \
	     -O3 -flto --closure 1 -o spasm.js \
	     -s EXPORTED_FUNCTIONS="['_main']" -s NO_EXIT_RUNTIME=1 \
	     -s EXPORTED_RUNTIME_METHODS="['callMain']" \
	     $(OBJ)
	 
clean: clean-dist
	rm -f $(OBJ) $(BINS)

clean-dist:
	rm -rf dist

