// Wrap the whole thing in a function to avoid polluting the global namespace
// with emscripten internals.
window['SPASM'] = (function() {
    function _log(t) {
        var span = document.createElement('span');
        span.innerHTML = t.replace(/[<>&]/, function(c) {
            return {
                '<': '&lt;',
                '>': '&gt;',
                '&': '&amp;'
            }[c];
        });
        window['log'].push(span);
        return span;
    }
    var Module = {
        // Setting noInitialRun skips things like FS initialization (preRun) functions,
        // which isn't useful.
        //'noInitialRun': true,
        'print': _log,
        'printErr': function(text) { _log(text)['className'] += ' error'; }
    };

