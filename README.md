This repository implements scaffolding to run
[SPASM-ng][spasm-ng] in the browser to build programs
for TI-8x (83+/84+) calculators.

[spasm-ng]: https://github.com/alberthdev/spasm-ng

[Emscripten][emscripten] is used to compile the C++
sources for SPASM into a format suitable for running in
a web browser, with some extra scripts and a little bit of
HTML to allow users to write code and get output.

[emscripten]: https://emscripten.org/

## Building

With GNU make, simply run `make`:

    make

All components needed to run the tool will be placed
in a `dist` directory, from where they can be served
with any web server or moved as desired.
