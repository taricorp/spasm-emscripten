    // Explicitly export everything we need outside this scope.
    // If compiled with --closure 1, everything in this scope gets mangled.
    return {
        'Module': Module,
        'FS': {
            'writeFile': FS.writeFile,
            'readFile': FS.readFile
        }
    };
})();
